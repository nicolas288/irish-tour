<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    #[Route('/', name:'app_index')]
    public function index() {
        return $this->render('index.html.twig');
    }

    #[Route('/team', name:'app_team')]
    public function team() {
        return $this->render('team.html.twig');
    }

    #[Route('/products', name:'app_products')]
    public function products() {
        return $this->render('products.html.twig');
    }

    #[Route('/contact', name:'app_contact')]
    public function contact() {
        return $this->render('contact.html.twig');
    }
}
